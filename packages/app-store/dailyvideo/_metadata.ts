import type { AppMeta } from "@calcom/types/App";

import _package from "./package.json";

export const metadata = {
  name: "Cradle Meet",
  description: _package.description,
  installed: !!process.env.DAILY_API_KEY,
  type: "daily_video",
  imageSrc: "/api/app-store/dailyvideo/Cradle_meet.svg",
  variant: "conferencing",
  url: "https://cradlemeet.com",
  trending: false,
  logo: "/api/app-store/dailyvideo/Cradle_meet.svg",
  publisher: "Cradle Schedule",
  verified: true,
  rating: 4.3, // TODO: placeholder for now, pull this from TrustPilot or G2
  reviews: 69, // TODO: placeholder for now, pull this from TrustPilot or G2
  category: "video",
  slug: "cradle-meet",
  title: "Cradle Meet",
  isGlobal: false,
  email: "help@usecradleapps.com",
  appData: {
    location: {
      linkType: "dynamic",
      type: "integrations:daily",
      label: "Cradle Meet",
    },
  },
  key: { apikey: process.env.DAILY_API_KEY },
} as AppMeta;

// export default metadata;
